/**
 * Sample React Native Router
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { createStackNavigator } from 'react-navigation';

import Home from './screens/Home'

const RootStack = createStackNavigator({
    Home: {
        screen: Home,
    },
},{
    initialRouteName: 'Home',
});

export default class App extends React.Component {
    render() {
        return <RootStack />;
    }
}
